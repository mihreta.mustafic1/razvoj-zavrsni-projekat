package api;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import static java.time.temporal.TemporalAdjusters.firstInMonth;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.exceptions.DatabaseException;

import entities.*;
import entities.User.AccountType;
import javafx.util.Pair;
import view.controllers.AlternativeDeanSceneController;

// svi metodi trebaju biti static
public class Api {
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("Zavrsni_projekat");
    private static EntityManager em = emf.createEntityManager();

	
	// returns a User if credentials are correct
	// returns null if they are not
	public static User login(String username, String password) {
		Query q = em.createQuery("SELECT u FROM User u WHERE u.username = ?1 and u.password = ?2");
		q.setParameter(1, username);
		q.setParameter(2, password);
		User user = null;
		try {
			user = (User)q.getSingleResult();
		}catch(Exception e) {
			user = null;
		}
		return user;
		
	}

	// GETTERS

	public static Collection<Classroom> getClassrooms(){
		Query q = em.createQuery("SELECT cl FROM Classroom cl");
		Collection<Classroom> data = new ArrayList<Classroom>();
		
		try {
			data = (Collection<Classroom>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Classroom>();
		}

		return data;
	}
	
	public static Collection<Group> getGroups(){
		Query q = em.createQuery("SELECT gr FROM Group gr");
		Collection<Group> data = new ArrayList<Group>();
		
		try {
			data = (Collection<Group>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Group>();
		}

		return data;
	}
	
	public static Collection<Building> getBuildings(){
		Query q = em.createQuery("SELECT b FROM Building b");
		Collection<Building> data = new ArrayList<Building>();
		
		try {
			data = (Collection<Building>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Building>();
		}
		
		return data;
	}
	
	public static Collection<Teacher> getTeachers(){
		Query q = em.createQuery("SELECT t FROM Teacher t");
		Collection<Teacher> data = new ArrayList<Teacher>();
		
		try {
			data = (Collection<Teacher>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Teacher>();
		}
		
		return data;
	}
	
	public static Collection<Subject> getSubjects(){
		Query q = em.createQuery("SELECT s FROM Subject s");
		Collection<Subject> data = new ArrayList<Subject>();
		
		try {
			data = (Collection<Subject>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Subject>();
		}
		
		return data;
	}
	
	public static Collection<Classroom> getClassroomsOfBuilding(Building b){
		Query q = em.createQuery("SELECT cl FROM Classroom cl WHERE cl.building.id = ?1");
		q.setParameter(1, b.getId());
		Collection<Classroom> classrooms = new ArrayList<Classroom>();
		
		try {
			classrooms = (Collection<Classroom>)q.getResultList();
		}catch(Exception e) {
			classrooms = new ArrayList<Classroom>();
		}

		return classrooms;
	}

	public static Collection<Cas> getCasovi(){
		Query q = em.createQuery("SELECT cas FROM Cas cas");
		Collection<Cas> data = new ArrayList<Cas>();
		
		try {
			data = (Collection<Cas>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Cas>();
		}
		
		return data;
	}

	public static Collection<Reservation> getReservations(){
		Query q = em.createQuery("SELECT r FROM Reservation r");
		Collection<Reservation> data = new ArrayList<Reservation>();
		
		try {
			data = (Collection<Reservation>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Reservation>();
		}
		
		return data;
	}
	
	// ovo mozda da se implementira u SQL preko WHERE, bilo bi brze vjerovatno
	public static Collection<Reservation> getReservationsInWeekOfDate(LocalDate date){
		Collection<Reservation> data = getReservations();
		
		Collection<Reservation> filteredData = new Vector<Reservation>();
		for(Reservation r : data) {
			if(		r.getDate().get(ChronoField.YEAR) == date.get(ChronoField.YEAR) &&

					r.getDate().get(WeekFields.of(Locale.GERMANY).weekOfWeekBasedYear()) == 
					date.get(WeekFields.of(Locale.GERMANY).weekOfWeekBasedYear())){
				filteredData.add(r);
			}
		}
		return filteredData;
	}

	public static Collection<Cas> getCasoviOfTeacher(Teacher teacher){
		Query q = em.createQuery("SELECT cas FROM Cas cas WHERE cas.group.teacher = ?1");
		q.setParameter(1, teacher);
		Collection<Cas> data = new ArrayList<Cas>();
		
		try {
			data = (Collection<Cas>)q.getResultList();
		}catch(Exception e) {
			data = new ArrayList<Cas>();
		}
		
		return data;
	}

	// Vraca casove koji su u mjesecu za datog profesora
	public static Collection<Pair<LocalDate, Cas>> getIzvjestajCasovi(Teacher teacher, LocalDate date)
	{
		Collection<Cas> casovi = getCasoviOfTeacher(teacher);
		Vector<Pair<LocalDate, Cas>> data = new Vector<Pair<LocalDate, Cas>>();

		LocalDate firstMonday 		= (date.with(firstInMonth(DayOfWeek.MONDAY)));
		LocalDate firstTuesday 		= (date.with(firstInMonth(DayOfWeek.TUESDAY)));
		LocalDate firstWednesday 	= (date.with(firstInMonth(DayOfWeek.WEDNESDAY)));
		LocalDate firstThursday 	= (date.with(firstInMonth(DayOfWeek.THURSDAY)));
		LocalDate firstFriday 		= (date.with(firstInMonth(DayOfWeek.FRIDAY)));
		LocalDate firstSaturday 	= (date.with(firstInMonth(DayOfWeek.SATURDAY)));


		for(Cas cas : casovi)
		{
			LocalDate d = null;

			switch(cas.getDay()) {
			case PON:
				d = firstMonday;
			case UTO:
				d = firstTuesday;
			case SRI:
				d = firstWednesday;
			case CET:
				d = firstThursday;
			case PET:
				d = firstFriday;
			case SUB:
				d = firstSaturday;
			}

			if(d == null)
				continue;

			while(d.getMonthValue() == date.getMonthValue()) 
			{
				data.add(new Pair<LocalDate, Cas>(d, cas));
				d = d.plusWeeks(1);
			}
		}
		data.sort((p1, p2) -> p1.getKey().compareTo(p2.getKey()));
		return data;
	}

	// INSERT
	public static void insertReservation(Reservation r) {
		// ovo je cisto da se pokaze da rad irefresh  u gui kad se ubaci novi
		em.getTransaction().begin();
		em.persist(r);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
	}
	
	public static void insertCas(Cas c) {
		// ovo je cisto da se pokaze da rad irefresh  u gui kad se ubaci novi
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
	}
	
	public static void insertBuilding(Building b) {
		// ovo je cisto da se pokaze da rad irefresh  u gui kad se ubaci novi
		em.getTransaction().begin();
		em.persist(b);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.reinitFilters();
	}

	public static void insertClassroom(Classroom c) {
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.reinitFilters();
	}
	
	public static void insertGroup(Group g) {
		em.getTransaction().begin();
		em.persist(g);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.reinitFilters();
	}
	
	public static void insertSubject(Subject s) {
		em.getTransaction().begin();
		em.persist(s);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.reinitFilters();
	}
	
	public static void insertTeacher(String name, String surname)
	{
		em.getTransaction().begin();
		Teacher t = new Teacher();
		t.setName(name);
		t.setSurname(surname);
		em.persist(t);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.reinitFilters();
	}

	public static void insertSubject(String name)
	{
		em.getTransaction().begin();
		Subject s = new Subject();
		s.setName(name);
		em.persist(s);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.reinitFilters();
	}

	public static boolean insertUser(Teacher teacher,String username, String password,AccountType type)
	{
		if(userExists(username))
			return false;
		em.getTransaction().begin();
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setTeacher(teacher);
		user.setType(type);
		em.persist(user);
		em.getTransaction().commit();
		return true;
	}
	// UPDATE
	
	public static void updateReservation(Reservation oldVal, Reservation newVal) {
		em.getTransaction().begin();

		newVal.setId(oldVal.getId());
		newVal.setTeacher(oldVal.getTeacher());
		em.merge(newVal);

		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
	}
	
	public static void updateBuilding(Building oldVal, Building newVal) {
		em.getTransaction().begin();

		newVal.setId(oldVal.getId());
		em.merge(newVal);

		em.getTransaction().commit();
       	AlternativeDeanSceneController.reinitFilters();
			
	}
	
	public static void updateCas(Cas oldVal, Cas newVal) {
		em.getTransaction().begin();

		newVal.setId(oldVal.getId());
		em.merge(newVal);

		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
			
	}
	
	public static void updateGroup(Group oldVal, Group newVal) {
		em.getTransaction().begin();

		newVal.setId(oldVal.getId());
		em.merge(newVal);

		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
			
	}
	
	public static void updateSubject(Subject oldVal, Subject newVal) {
		em.getTransaction().begin();

		newVal.setId(oldVal.getId());
		em.merge(newVal);

		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
			
	}
	
	public static void updateClassroom(Classroom oldVal, Classroom newVal) {
		em.getTransaction().begin();

		newVal.setId(oldVal.getId());
		em.merge(newVal);

		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
			
	}
	
	public static void updateTeacher(Teacher oldVal, Teacher newVal) {
		em.getTransaction().begin();

		newVal.setId(oldVal.getId());
		em.merge(newVal);

		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
			
	}
	
	// DELETE
	
	public enum DatabaseError{
		OK,
		IntegrityViolation,
		Unknown
	}
	
	public static void deleteReservation(Reservation r) {
		em.getTransaction().begin();
		em.remove(r);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
	}

	public static void deleteUser(User u)
	{	
//		em.getTransaction().begin();
//		em.remove(u);
//		em.getTransaction().commit();
//		AlternativeDeanSceneController.refreshRaspored();
	}
	
	public static DatabaseError deleteBuilding(Building b) {
			return DatabaseError.OK;
//		try{
//			em.getTransaction().begin();
//			em.remove(b);
//			em.getTransaction().commit();
//			AlternativeDeanSceneController.reinitFilters();
//			return DatabaseError.OK;
//		}catch(Throwable e) {
//				return DatabaseError.IntegrityViolation;
//		}
	}
	
	public static DatabaseError deleteTeacher(Teacher t) {
			return DatabaseError.OK;
//		try{
//			em.getTransaction().begin();
//			em.remove(t);
//			em.getTransaction().commit();
//			AlternativeDeanSceneController.reinitFilters();
//			return DatabaseError.OK;
//		}catch(Throwable e) {
//				return DatabaseError.IntegrityViolation;
//		}
	}
	
	public static DatabaseError deleteSubject(Subject s) {
			return DatabaseError.OK;
//		try{
//			em.getTransaction().begin();
//			em.remove(s);
//			em.getTransaction().commit();
//			AlternativeDeanSceneController.reinitFilters();
//			return DatabaseError.OK;
//		}catch(Throwable e) {
//				return DatabaseError.IntegrityViolation;
//		}
	}
	
	public static void deleteCas(Cas c) {
		em.getTransaction().begin();
		em.remove(c);
		em.getTransaction().commit();
       	AlternativeDeanSceneController.refreshRaspored();
	}
	public static DatabaseError deleteClassroom(Classroom c) {
			return DatabaseError.OK;
//		try{
//			em.getTransaction().begin();
//			em.remove(c);
//			em.getTransaction().commit();
//			AlternativeDeanSceneController.reinitFilters();
//			return DatabaseError.OK;
//		}catch(Throwable e) {
//				return DatabaseError.IntegrityViolation;
//		}
	}
	public static DatabaseError deleteGroup(Group g) {
			return DatabaseError.OK;
//		try{
//			em.getTransaction().begin();
//			em.remove(g);
//			em.getTransaction().commit();
//			AlternativeDeanSceneController.reinitFilters();
//			return DatabaseError.OK;
//		}catch(Throwable e) {
//				return DatabaseError.IntegrityViolation;
//		}
	}
	// FILTERS

	public static Collection<Cas> filterCasByBuilding(Collection<Cas> casovi, Building building){
		Collection<Cas> filteredCasovi = new Vector<Cas>();
		for(Cas cas : casovi) {
			if(cas.getClassroom().getBuilding().getId() == building.getId()) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}

	public static Collection<Cas> filterCasByClassroom(Collection<Cas> casovi, Classroom classroom){
		Collection<Cas> filteredCasovi = new Vector<Cas>();
		for(Cas cas : casovi) {
			if(cas.getClassroom().getId() == classroom.getId()) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}
	public static Collection<Cas> filterCasByProfessor(Collection<Cas> casovi, Teacher teacher){
		Collection<Cas> filteredCasovi = new Vector<Cas>();
		for(Cas cas : casovi) {
			if(cas.getGroup().getTeacher().getId() == teacher.getId()) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}
	public static Collection<Cas> filterCasBySemester(Collection<Cas> casovi, String semester){
		Collection<Cas> filteredCasovi = new Vector<Cas>();
		for(Cas cas : casovi) {
			if(cas.getGroup().getSemester() == semester) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}
	public static Collection<Cas> filterCasByYear(Collection<Cas> casovi, String year){
		String semester1 = new String("I");
		String semester2 = new String("I");
		if(year.equals("I")) {
			semester1 = new String("I");
			semester2 = new String("II");
		}
		else if(year.equals("II")) {
			semester1 = new String("III");
			semester2 = new String("IV");
		}
		else if(year.equals("III")) {
			semester1 = new String("V");
			semester2 = new String("VI");
		}
		else if(year.equals("IV")) {
			semester1 = new String("VII");
			semester2 = new String("VIII");
		}

		Collection<Cas> filteredCasovi = new Vector<Cas>();
		for(Cas cas : casovi) {
			System.out.println(cas.getGroup().getSemester());
			if(cas.getGroup().getSemester().equals(semester1) ||
					cas.getGroup().getSemester().equals(semester2)) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}

	public static Collection<Cas> filterCasBySubject(Collection<Cas> casovi, Subject subject){
		Collection<Cas> filteredCasovi = new Vector<Cas>();
		for(Cas cas : casovi) {
			if(cas.getGroup().getSubject().getId() == subject.getId()) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}
	public static Collection<Cas> filterCasByGroup(Collection<Cas> casovi, Group.GroupType groupType){
		Collection<Cas> filteredCasovi = new Vector<Cas>();
		for(Cas cas : casovi) {
			if(cas.getGroup().getType() == groupType) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}

	//
	// FILTERS FOR RESERVATION
	// 
	public static Collection<Reservation> filterReservationByBuilding(Collection<Reservation> casovi, Building building){
		Collection<Reservation> filteredCasovi = new Vector<Reservation>();
		for(Reservation cas : casovi) {
			if(cas.getClassroom().getBuilding().getId() == building.getId()) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}

	public static Collection<Reservation> filterReservationByClassroom(Collection<Reservation> casovi, Classroom classroom){
		Collection<Reservation> filteredCasovi = new Vector<Reservation>();
		for(Reservation cas : casovi) {
			if(cas.getClassroom().getId() == classroom.getId()) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}
	public static Collection<Reservation> filterReservationByProfessor(Collection<Reservation> casovi, Teacher teacher){
		Collection<Reservation> filteredCasovi = new Vector<Reservation>();
		for(Reservation cas : casovi) {
			if(cas.getTeacher().getId() == teacher.getId()) {
				filteredCasovi.add(cas);
			}
		}
		return filteredCasovi;
	}
	public static Collection<Reservation> filterReservationBySemester(Collection<Reservation> casovi, String semester){
		return casovi;
	}
	public static Collection<Reservation> filterReservationByYear(Collection<Reservation> casovi, String year){
		return casovi;
	}

	public static Collection<Reservation> filterReservationBySubject(Collection<Reservation> casovi, Subject subject){
		return casovi;
	}
	public static Collection<Reservation> filterReservationByGroup(Collection<Reservation> casovi, Group.GroupType groupType){
		return casovi;
	}

	public static boolean userExists(String username)
	{
		int size = 0;
		Query q = em.createQuery("SELECT u FROM User u WHERE u.username = ?1");
		q.setParameter(1, username);
		Collection<User> users = new ArrayList<User>();
		
		try {
			users = (Collection<User>)q.getResultList();
		}catch(Exception e) {
			users = new ArrayList<User>();
		}
		if(users.size()>0)
			return true;
		return false;
	}

	
	
	// ###################################################################
	// TEST
	// ###################################################################
	// ovdje ubacivat test data jer ce svaki put biti drop n create DB
	// i brise se citava baza svaki put kad se pokrene program
	// slobodno nek bude kilometarska funkcija al nek stoji dole na kraju
	// da ne zauzima gore bitni prostor
	// mozda ovdje da napravimo da iz fajla cita dok debuggiramo?
	// ###################################################################
	public static void insertTestData() {
		em.getTransaction().begin();
		
		
		// BUILDINGS
		
		Building fet = new Building();
		fet.setName("FET");
		em.persist(fet);
		
		Building gim = new Building();
		gim.setName("Gimnazija");
		em.persist(gim);
		
		// CLASSROOMS


		Classroom cl = new Classroom();
		cl.setBuilding(fet);
		cl.setName("A008");
		em.persist(cl);
		
		Classroom cl1 = new Classroom();
		cl1.setBuilding(fet);
		cl1.setName("A005");
		em.persist(cl1);
		
		Classroom cl2 = new Classroom();
		cl2.setBuilding(fet);
		cl2.setName("RC14");
		em.persist(cl2);
		
		Classroom cl3 = new Classroom();
		cl3.setBuilding(gim);
		cl3.setName("G-RC20");
		em.persist(cl3);
		
		// TEACHERS

		Teacher t1 = new Teacher();
		t1.setName("Amer");
		t1.setSurname("Hasanovic");
		em.persist(t1);
		
		Teacher t2 = new Teacher();
		t2.setName("Emir");
		t2.setSurname("Meskovic");
		em.persist(t2);
		
		Teacher t3 = new Teacher();
		t3.setName("Edin");
		t3.setSurname("Pjanic");
		em.persist(t3);
		
		Teacher t4 = new Teacher();
		t4.setName("Ermin");
		t4.setSurname("Katardzic");
		em.persist(t4);
		
		Teacher t5 = new Teacher();
		t5.setName("Edin");
		t5.setSurname("Selimovic");
		em.persist(t5);

		// USERS

		User mesak = new User();
		mesak.setUsername("prodekan");
		mesak.setPassword("");
		mesak.setTeacher(t2);
		mesak.setType(User.AccountType.Dean);
		em.persist(mesak);
		
		User prof = new User();
		prof.setUsername("profesor");
		prof.setPassword("");
		prof.setTeacher(t1);
		prof.setType(User.AccountType.Professor);
		em.persist(prof);
		
		// SUBJECTS
		Subject s1 = new Subject();
		s1.setName("Objektno Orjentisano Programiranje");
		em.persist(s1);
		
		Subject s2 = new Subject();
		s2.setName("Baze Podataka");
		em.persist(s2);
		
		Subject s3 = new Subject();
		s3.setName("Dizajn Kompajlera");
		em.persist(s3);

		Subject s4 = new Subject();
		s4.setName("Arhitektura Racunara");
		em.persist(s4);

		// GROUPS
		Group g = new Group();
		g.setType(Group.GroupType.PRED);
		g.setSubject(s1);
		g.setTeacher(t1);
		g.setSemester("I");
		em.persist(g);

		Group g1 = new Group();
		g1.setType(Group.GroupType.AV);
		g1.setSubject(s1);
		g1.setTeacher(t4);
		g1.setSemester("I");
		em.persist(g1);

		Group g2 = new Group();
		g2.setType(Group.GroupType.PRED);
		g2.setSubject(s3);
		g2.setTeacher(t3);
		g2.setSemester("I");
		em.persist(g2);

		Group g3 = new Group();
		g3.setType(Group.GroupType.AV);
		g3.setSubject(s1);
		g3.setTeacher(t4);
		g3.setSemester("VI");
		em.persist(g3);

		Group g4 = new Group();
		g4.setType(Group.GroupType.AV);
		g4.setSubject(s3);
		g4.setTeacher(t5);
		g4.setSemester("I");
		em.persist(g4);

		Group g5 = new Group();
		g5.setType(Group.GroupType.PRED);
		g5.setSubject(s4);
		g5.setTeacher(t1);
		g5.setSemester("II");
		em.persist(g5);
		
		// CASOVI
		Cas c1 = new Cas();	
		c1.setGroup(g);
		c1.setClassroom(cl1);
		c1.setDay(Cas.Day.PON);
		c1.setStartHour(17);
		c1.setDuration(3);
		em.persist(c1);

		Cas c2 = new Cas();	
		c2.setGroup(g2);
		c2.setClassroom(cl3);
		c2.setDay(Cas.Day.UTO);
		c2.setStartHour(11);
		c2.setDuration(3);
		em.persist(c2);
		
		Cas c3 = new Cas();	
		c3.setGroup(g1);
		c3.setClassroom(cl2);
		c3.setDay(Cas.Day.PON);
		c3.setStartHour(8);
		c3.setDuration(3);
		em.persist(c3);

		Cas c4 = new Cas();	
		c4.setGroup(g4);
		c4.setClassroom(cl2);
		c4.setDay(Cas.Day.PON);
		c4.setStartHour(10);
		c4.setDuration(2);
		em.persist(c4);
		
		Cas c5 = new Cas();	
		c5.setGroup(g5);
		c5.setClassroom(cl3);
		c5.setDay(Cas.Day.PET);
		c5.setStartHour(8);
		c5.setDuration(3);
		em.persist(c5);

		// Reservations
		
		Reservation r1 = new Reservation();
		r1.setTeacher(t1);
		r1.setClassroom(cl1);
		r1.setDecription("Nadonkada za nesto");
		r1.setDate(LocalDate.of(2019, 10, 10));
		r1.setStartHour(15);
		r1.setDuration(3);
		em.persist(r1);

		Reservation r2 = new Reservation();
		r2.setTeacher(t3);
		r2.setClassroom(cl3);
		r2.setDecription("Simpozij");
		r2.setDate(LocalDate.of(2019, 10, 8));
		r2.setStartHour(10);
		r2.setDuration(2);
		em.persist(r2);
		
		Usmjerenje ri = new Usmjerenje();
		ri.setName("RI");
		ri.addSubject(s1);
//		em.persist(ri);
		System.out.println("+++" + ri.getSubjects().size());
		
		em.getTransaction().commit();
	}
}
