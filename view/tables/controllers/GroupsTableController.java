package view.tables.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import api.Api;
import api.Api.DatabaseError;
import entities.Classroom;
import entities.Group;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;
import view.dialogs.controllers.EditGroupController;

public class GroupsTableController implements Initializable {
	@FXML
	private TableView<Group> tableView;
    @FXML
	private TableColumn<Group, String> typeColumn;
	@FXML
	private TableColumn<Group, String> teacherColumn;
	@FXML
	private TableColumn<Group, String> subjectColumn;
	@FXML
	private TableColumn<Group, String> semesterColumn;
	@FXML
	private Button btnAddItem;
	@FXML
	private Button btnDeleteItem;
	@FXML
	private Label lblError;

	
	
	 @Override
	 public void initialize(URL url, ResourceBundle rb) {
            teacherColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getTeacher().toString());});
	        subjectColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getSubject().getName());});
	        typeColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getType().toString());});
	        semesterColumn.setCellValueFactory(cellData -> {return new ReadOnlyStringWrapper(cellData.getValue().getSemester());});

	        tableView.setItems(FXCollections.observableArrayList(Api.getGroups()));
	        tableView.setEditable(true);
	        
            tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            
        }    
	 
		@FXML
	    public void handleEdit(CellEditEvent edittedCell){
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)tableView.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editGroup.fxml"));
		        Parent root = loader.load();
		   	    EditGroupController editGroupController = loader.getController();
		        editGroupController.setGroup(tableView.getSelectionModel().getSelectedItem());
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Uredi grupu");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
	    }
		
		@FXML 
		public void handleBtnAddItem() {
			try {
				Stage dialog = new Stage();
				dialog.initModality(Modality.NONE);
				Stage appStage=(Stage)btnAddItem.getScene().getWindow();
				dialog.initOwner(appStage);
		
			    FXMLLoader loader = new FXMLLoader(getClass().getResource("../../dialogs/editGroup.fxml"));
		        Parent root = loader.load();
		        Scene scene=new Scene(root);
		        dialog.setScene(scene);
		        dialog.setTitle("Dodaj grupu");
		        dialog.show();
		        dialog.setOnCloseRequest(event -> { rerender();});
			}
			catch(Throwable e) {
				System.out.println(e);}
		}
		
		private void rerender() {
	        tableView.setItems(FXCollections.observableArrayList(Api.getGroups()));
		}
		@FXML
		public void handleBtnDeleteItem() {
		ObservableList<Group> selectedRows = tableView.getSelectionModel().getSelectedItems();

		       if(selectedRows.size() != 0) {
				   Main.showConfirmDialog("Da li ste sigurni da zelite izbrisati grupu?",
							bool -> {
								if(bool) {
									    for (Group row : selectedRows) {
									    	try {DatabaseError e = Api.deleteGroup(row);
									    	if(e.equals(DatabaseError.OK))
											     rerender();
										    	else
										    	 lblError.setText("Nemoguce izbrisati grupu, koristi se u drugim entitetima u bazi!");	}
									    	catch(Exception e) {
									    		e.printStackTrace();
									    	}
								       
									    }
								}});
			            }
			
		 }
}
